# README #

Please follow the next steps to run the scripts.

### IMPORT ###

# GRUPO MONITOREOS #

* mongoimport -d umaeye -c grupomonitoreos --drop --type csv --file grupo_monitoreo.csv --headerline

# ITEMS CLASIFICADOR GENERAL #

* mongoimport -d umaeye -c itemclasificadorgenerals --drop --type csv --file item_clasificador_general.csv --headerline